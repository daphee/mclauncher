package auth

import (
	"bitbucket.org/daphee/mclauncher/util"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type AuthenticationRequest struct {
	Agent       agent  `json:"agent"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	ClientToken string `json:"clientToken"`
}

type agent struct {
	Name    string `json:"name"`
	Version int    `json:"version"`
}

type AuthenticationResponse struct {
	AccessToken     string
	SelectedProfile profile
}

type profile struct {
	Id, Name string
	Legacy   bool
}

var mcAgent = agent{Name: "Minecraft", Version: 1}

func Authenticate(username, password string) (*AuthenticationResponse, error) {
	req := AuthenticationRequest{
		Agent:       mcAgent,
		Username:    username,
		Password:    password,
		ClientToken: util.Settings.ClientToken,
	}

	resp := &AuthenticationResponse{}

	buf := &bytes.Buffer{}

	if err := json.NewEncoder(buf).Encode(req); err != nil {
		return resp, err
	}

	r, err := http.Post("https://authserver.mojang.com/authenticate", "application/json", buf)
	if err != nil {
		return resp, err
	}
	defer r.Body.Close()

	if r.StatusCode != 200 {
		return resp, errors.New(fmt.Sprintf("authenticate status code:%d", r.StatusCode))
	}

	err = json.NewDecoder(r.Body).Decode(resp)
	return resp, err
}
