package install

import (
	"archive/zip"
	"bitbucket.org/daphee/mclauncher/minecraft"
	"bitbucket.org/daphee/mclauncher/util"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
)

func exists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func InstallMCVersion(version *minecraft.MinecraftVersion) error {
	versionPath := path.Join(util.RootFolder, "versions", version.Id)
	if err := os.MkdirAll(versionPath, 0755); err != nil {
		return err
	}

	jarPath := path.Join(versionPath, version.Id+".jar")
	versionFilePath := path.Join(versionPath, version.Id+".json")
	if !exists(jarPath) {
		if _, err := util.DownloadFile(version.GetJarUrl(), jarPath); err != nil {
			return err
		}
	}
	if !exists(versionFilePath) {
		if _, err := util.DownloadFile(version.GetVersionUrl(), versionFilePath); err != nil {
			return err
		}
	}

	if changed, err := InstallLibraries(version.Libraries); err != nil {
		return err
	} else if changed {
		fmt.Println("Libraries changed -> Rebuilding native folder")
		nativePath := path.Join(util.RootFolder, "versions", version.Id, "natives")

		//clear the native folder
		if exists(nativePath) {
			if err := os.RemoveAll(nativePath); err != nil {
				return err
			}
		}
		if err := os.MkdirAll(nativePath, 0755); err != nil {
			return err
		}

		for _, lib := range version.Libraries {
			if !lib.IsAllowed() {
				continue
			}
			//if we shoudl extract the lib
			if lib.Extract != nil {
				libraryPath := path.Join(path.Join(util.RootFolder, "libraries"), lib.GetPath())

				z, err := zip.OpenReader(libraryPath)
				if err != nil {
					return err
				}

				exclude := []string{}

				//stupid conversion of interface{} to []string
				if obj, ok := lib.Extract["exclude"]; ok {
					if arr, ok := obj.([]interface{}); ok {
						for _, arrObj := range arr {
							if s, ok := arrObj.(string); ok {
								exclude = append(exclude, s)
							}
						}
					}
				}

				for _, f := range z.File {
					match := false
					for _, rule := range exclude {
						//check if filename begins with rule
						if strings.Index(f.Name, rule) == 0 {
							match = true
							break
						}
					}
					if match {
						continue
					}

					extractFilePath := path.Join(nativePath, f.Name)
					if err := os.MkdirAll(path.Dir(extractFilePath), 0755); err != nil {
						return nil
					}

					reader, err := f.Open()
					if err != nil {
						return err
					}
					defer reader.Close()

					writer, err := os.Create(extractFilePath)
					if err != nil {
						return err
					}
					defer writer.Close()

					if _, err := io.Copy(writer, reader); err != nil {
						return err
					}

					fmt.Println("Extracted", f.Name)
				}
			}
		}
	} else {
		fmt.Println("No need to recreate native folder")
	}

	if err := InstallAssets(version); err != nil {
		return err
	}

	return nil
}

func InstallLibraries(libs []*minecraft.Library) (bool, error) {
	librariesPath := path.Join(util.RootFolder, "libraries")
	if err := os.MkdirAll(librariesPath, 0755); err != nil {
		return false, err
	}

	changed := false

	for _, lib := range libs {
		if !lib.IsAllowed() {
			continue
		}

		libraryUrl, libraryPath := lib.GetDownloadUrl(), path.Join(librariesPath, lib.GetPath())
		if exists(libraryPath) && exists(libraryPath+".sha") {
			if ok, err := util.CheckFile(libraryPath); err != nil {
				return false, err
			} else if ok {
				fmt.Println("Skipping", lib.Name)
				continue
			}
		}

		if err := os.MkdirAll(path.Dir(libraryPath), 0755); err != nil {
			return false, err
		}

		for i := 1; i < 4; i++ {
			if _, err := util.DownloadFile(libraryUrl, libraryPath); err != nil {
				if i == 3 {
					return false, err
				} else {
					continue
				}
			}

			if _, err := util.DownloadFile(libraryUrl+".sha1", libraryPath+".sha"); err != nil && i == 3 {
				if i == 3 {
					return false, err
				} else {
					continue
				}
			}
			changed = true
			break
		}
	}

	return changed, nil
}

func InstallAssets(version *minecraft.MinecraftVersion) error {
	assets, virtual, err := minecraft.GetAssets(version.Assets)
	if err != nil {
		return err
	}

	version.VirtualAssets = virtual

	assetFolderPath := path.Join(util.RootFolder, "assets")
	if virtual {
		assetFolderPath = path.Join(assetFolderPath, "virtual")
	} else {
		assetFolderPath = path.Join(assetFolderPath, "objects")
	}

	if err := os.MkdirAll(assetFolderPath, 0755); err != nil {
		return err
	}

	for _, asset := range assets {
		assetUrl := asset.GetUrl()
		assetPath := ""
		if virtual {
			assetPath = path.Join(assetFolderPath, asset.Name)
		} else {
			assetPath = path.Join(assetFolderPath, asset.GetPath())
		}

		if err := os.MkdirAll(path.Dir(assetPath), 0755); err != nil {
			return err
		}

		if exists(assetPath) {
			if sum, err := util.SumFile(assetPath); err != nil {
				return err
			} else if sum == asset.Hash {
				fmt.Println("Skipping", asset.Name)
				continue
			}
		}

		for i := 1; i < 4; i++ {
			if num, err := util.DownloadFile(assetUrl, assetPath); err != nil {
				if i == 3 {
					return err
				} else {
					continue
				}
			} else if num != asset.Size && i == 3 {
				if i == 3 {
					return errors.New(fmt.Sprintf("downloading asset '%s' failed. File too small", asset.Name))
				} else {
					continue
				}
			}

			if sum, err := util.SumFile(assetPath); err != nil && i == 3 {
				if i == 3 {
					return err
				} else {
					continue
				}
			} else if sum != asset.Hash {
				if i == 3 {
					return err
				} else {
					continue
				}
			}
			break
		}
	}

	return nil
}
