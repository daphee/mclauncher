package launch

import (
	"bitbucket.org/daphee/mclauncher/auth"
	"bitbucket.org/daphee/mclauncher/install"
	"bitbucket.org/daphee/mclauncher/minecraft"
	"bitbucket.org/daphee/mclauncher/util"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func LaunchVersion(id string) error {
	util.LoadSettings("./data")

	version := &minecraft.MinecraftVersion{Id: id}

	if err := version.FetchDetails(); err != nil {
		return err
	}

	if err := install.InstallMCVersion(version); err != nil {
		return err
	}

	cmd := exec.Command("java")
	cmd.Args = append(cmd.Args, "-Djava.library.path="+version.GetNativePath())
	cmd.Args = append(cmd.Args, "-cp")
	cmd.Args = append(cmd.Args, version.GetClassPath())
	cmd.Args = append(cmd.Args, version.MainClass)

	session, err := auth.Authenticate("felix.scheinost@gmail.com", "felix2006")
	if err != nil {
		return err
	}

	gameDir, _ := filepath.Abs("./data/gamedir")
	userType := ""
	if session.SelectedProfile.Legacy {
		userType = "legacy"
	} else {
		userType = "mojang"
	}
	minecraftArgs := map[string]string{
		"auth_player_name":  session.SelectedProfile.Name,
		"version_name":      version.Id,
		"game_directory":    gameDir,
		"assets_root":       version.GetAssetPath(),
		"assets_index_name": version.Assets,
		"auth_uuid":         session.SelectedProfile.Id,
		"auth_access_token": session.AccessToken,
		"user_type":         userType,
		"user_properties":   "{}",
	}

	mcArgs := util.ReplaceArguments(version.MinecraftArguments, minecraftArgs)
	for _, arg := range strings.Split(mcArgs, " ") {
		cmd.Args = append(cmd.Args, arg)
	}
	fmt.Println(cmd.Args)

	cmd.Args = append(cmd.Args, mcArgs)

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Dir, _ = filepath.Abs(util.RootFolder)

	cmd.Run()

	return nil
}
