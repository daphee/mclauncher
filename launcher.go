package main

import (
	"bitbucket.org/daphee/mclauncher/launch"
	"fmt"
)

func main() {
	fmt.Println(launch.LaunchVersion("1.6.4"))
}
