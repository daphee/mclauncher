package minecraft

import (
	"bitbucket.org/daphee/mclauncher/util"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
)

type Asset struct {
	Name, Hash string
	Size       int
}

type assetIndex struct {
	Objects map[string]*Asset
	Virtual bool
}

type assetsNotIndexedError struct {
	error
}

func GetWebAssets(name string) ([]*Asset, bool, error) {
	r := []*Asset{}

	resp, err := http.Get(fmt.Sprintf("https://s3.amazonaws.com/Minecraft.Download/indexes/%s.json", name))
	if err != nil {
		return r, false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 403 {
		return r, false, errors.New(fmt.Sprintf("asset index '%s' doesn't exist", name))
	} else if resp.StatusCode != 200 {
		return r, false, errors.New(fmt.Sprintf("couldn't fetch asset index '%s'(status code:%d)", name, resp.StatusCode))
	}

	indexPath := path.Join(util.RootFolder, "assets", "indexes", name+".json")
	if err := os.MkdirAll(path.Dir(indexPath), 0755); err != nil {
		return r, false, err
	}

	f, err := os.Create(indexPath)
	if err != nil {
		return r, false, err
	}
	defer f.Close()

	io.Copy(f, resp.Body)

	index := &assetIndex{}

	f.Seek(0, 0)
	if err := json.NewDecoder(f).Decode(index); err != nil {
		return r, false, nil
	}

	for name, a := range index.Objects {
		a.Name = name
		r = append(r, a)
	}

	return r, index.Virtual, nil
}

func GetAssets(name string) ([]*Asset, bool, error) {
	r := []*Asset{}
	indexPath := path.Join(util.RootFolder, "assets", "indexes", name+".json")

	if _, err := os.Stat(indexPath); os.IsNotExist(err) {
		return GetWebAssets(name)
	}

	f, err := os.Open(indexPath)
	if err != nil {
		return r, false, err
	}
	defer f.Close()

	index := &assetIndex{}

	if err := json.NewDecoder(f).Decode(index); err != nil {
		return r, false, nil
	}

	for name, a := range index.Objects {
		a.Name = name
		r = append(r, a)
	}

	return r, index.Virtual, nil

}

func (a *Asset) GetPath() string {
	return a.Hash[0:2] + "/" + a.Hash
}

func (a *Asset) GetUrl() string {
	return "http://resources.download.minecraft.net/" + a.GetPath()
}
