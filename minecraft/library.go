package minecraft

import (
	"bitbucket.org/daphee/mclauncher/util"
	"fmt"
	"strings"
)

type Library struct {
	Name    string
	Natives map[string]string
	Extract map[string]interface{}
	Rules   []*rule
}

type rule struct {
	Action string
	Os     map[string]string
}

func (l *Library) GetPath() string {
	nameParts := strings.Split(l.Name, ":")

	native := ""
	if l.Natives != nil {
		if val, ok := l.Natives[util.GetOs()]; ok {
			native = "-" + val
			native = strings.Replace(native, "${arch}", util.GetArch(), 1)
		}
	}

	//%name/%package/%version/%package-%version(-%native).jar
	return fmt.Sprintf("%s/%s/%s/%s-%s%s.jar", strings.Replace(nameParts[0], ".", "/", -1), nameParts[1],
		nameParts[2], nameParts[1], nameParts[2], native)
}

func (l *Library) GetDownloadUrl() string {
	return "https://libraries.minecraft.net/" + l.GetPath()
}

func (l *Library) IsAllowed() bool {
	if l.Rules == nil {
		return true
	}

	d := false
	for _, rule := range l.Rules {
		if rule.Os == nil {
			if rule.Action == "allow" {
				d = true
			}
		} else {
			if rule.Os["name"] == util.GetOs() {
				return rule.Action == "allowed"
			}
		}
	}
	return d
}
