package minecraft

import (
	"bitbucket.org/daphee/mclauncher/util"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
)

type MinecraftVersion struct {
	Id                     string
	Type                   string
	MinecraftArguments     string
	MinimumLauncherVersion int
	Assets                 string
	Libraries              []*Library
	MainClass              string
	detailed               bool
	VirtualAssets          bool
}

type versionsResponse struct {
	Versions []*MinecraftVersion
}

func GetVersions() ([]*MinecraftVersion, error) {
	r, err := WebGetVersions()
	/*if urlErr, ok := err.(*url.Error); ok {
		if netErr, ok := urlErr.Err.(*net.OpError); ok {
			if !netErr.Temporary() {
				//Client seems offline
				rr := LocalGetVersions()
				return r, err
			}
		}
	}*/
	return r, err
}

func WebGetVersions() ([]*MinecraftVersion, error) {
	r := []*MinecraftVersion{}

	resp, err := http.Get("http://s3.amazonaws.com/Minecraft.Download/versions/versions.json")
	if err != nil {
		return r, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return r, errors.New(fmt.Sprintf("couldn't fetch versions (status code:%d)", resp.StatusCode))
	}

	versions := &versionsResponse{}

	if err := json.NewDecoder(resp.Body).Decode(versions); err != nil {
		return r, err
	}

	for _, version := range versions.Versions {
		if version.Type == "release" {
			r = append(r, version)
		}
	}

	return r, nil
}

/*func LocalGetVersions() []*MinecraftVersion {

}*/

func (v *MinecraftVersion) FetchDetails() error {
	if v.detailed {
		return nil
	}

	if v.Id == "" {
		return errors.New(fmt.Sprintf("can't fetch details for '%s'", v.Id))
	}

	err := v.WebFetchDetails()
	if urlErr, ok := err.(*url.Error); ok {
		if netErr, ok := urlErr.Err.(*net.OpError); ok {
			if !netErr.Temporary() {
				//Client seems offline
				return v.LocalFetchDetails()
			}
		}
	}
	return err
}

func (v *MinecraftVersion) LocalFetchDetails() error {
	versionPath := path.Join(util.RootFolder, "versions", v.Id, v.Id+".json")
	if _, err := os.Stat(versionPath); !os.IsNotExist(err) {
		f, err := os.Open(versionPath)
		if err != nil {
			return err
		}
		defer f.Close()

		if err := json.NewDecoder(f).Decode(v); err != nil {
			return err
		}

		v.detailed = true
		return nil
	} else {
		return errors.New(fmt.Sprintf("you need to be online to install this version"))
	}
}

func (v *MinecraftVersion) WebFetchDetails() error {
	resp, err := http.Get(v.GetVersionUrl())
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		if resp.StatusCode == 403 {
			return errors.New(fmt.Sprintf("version '%s' not found", v.Id))
		}
		return errors.New(fmt.Sprintf("couldn't fetch version '%s' (status code:%d)", v.Id, resp.StatusCode))
	}

	if err := json.NewDecoder(resp.Body).Decode(v); err != nil {
		return err
	}

	v.detailed = true
	return nil
}

func (v *MinecraftVersion) GetVersionUrl() string {
	return fmt.Sprintf("http://s3.amazonaws.com/Minecraft.Download/versions/%s/%s.json", v.Id, v.Id)
}

func (v *MinecraftVersion) GetJarUrl() string {
	return fmt.Sprintf("http://s3.amazonaws.com/Minecraft.Download/versions/%s/%s.jar", v.Id, v.Id)
}

func (v *MinecraftVersion) GetNativePath() string {
	p, _ := filepath.Abs(path.Join(util.RootFolder, "versions", v.Id, "natives"))
	return p
}

func (v *MinecraftVersion) GetClassPath() string {
	cp := ""
	for _, lib := range v.Libraries {
		if !lib.IsAllowed() {
			continue
		}

		libPath, err := filepath.Abs(path.Join(util.RootFolder, "libraries", lib.GetPath()))
		if err != nil {
			continue
		}
		if cp != "" {
			cp += ":"
		}
		cp += libPath
	}
	jarPath, _ := filepath.Abs(path.Join(util.RootFolder, "versions", v.Id, v.Id+".jar"))
	cp += ":" + jarPath
	return cp
}

func (v *MinecraftVersion) GetAssetPath() string {
	assetPath := path.Join(util.RootFolder, "assets")
	if v.VirtualAssets {
		assetPath = path.Join(assetPath, "virtual")
	}
	p, _ := filepath.Abs(assetPath)
	return p
}
