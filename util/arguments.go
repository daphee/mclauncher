package util

import (
	"strings"
)

func ReplaceArguments(s string, args map[string]string) string {
	for name, value := range args {
		s = strings.Replace(s, "${"+name+"}", value, -1)
	}
	return s
}
