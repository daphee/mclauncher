package util

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"io"
	"os"
)

func exists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func SumFile(path string) (string, error) {
	sum := []byte{}
	h := sha1.New()

	f, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer f.Close()

	if _, err := io.Copy(h, f); err != nil {
		return "", err
	}

	return hex.EncodeToString(h.Sum(sum)), nil
}

func CheckFile(path string) (bool, error) {
	if !exists(path) || !exists(path+".sha") {
		return false, errors.New(path + " or " + path + ".sha not existing")
	}

	sum, err := SumFile(path)
	if err != nil {
		return false, err
	}

	f, err := os.Open(path + ".sha")
	if err != nil {
		return false, err
	}
	defer f.Close()

	b := &bytes.Buffer{}
	if _, err := io.Copy(b, f); err != nil {
		return false, err
	}
	return sum == b.String(), nil
}
