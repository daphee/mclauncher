package util

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
)

func DownloadFile(url, path string) (int, error) {
	fmt.Println("Downloading", url, "to", path)
	resp, err := http.Get(url)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return 0, errors.New(fmt.Sprintf("couldn't fetch '%s' status code:%d", url, resp.StatusCode))
	}

	f, err := os.Create(path)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	n, err := io.Copy(f, resp.Body)
	return int(n), err
}
