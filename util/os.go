package util

import (
	"runtime"
)

func GetOs() string {
	os := runtime.GOOS
	if os == "darwin" {
		return "osx"
	}
	return os
}

func GetArch() string {
	if runtime.GOARCH == "386" {
		return "32"
	} else if runtime.GOARCH == "amd64" {
		return "64"
	} else {
		return "unknown"
	}
}
