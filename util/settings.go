package util

import (
	"code.google.com/p/go-uuid/uuid"
	"encoding/json"
	"os"
	"path"
)

type settings struct {
	ClientToken string
}

var Settings settings
var RootFolder string

func LoadSettings(rootFolder string) error {
	RootFolder = rootFolder

	settingsPath := path.Join(rootFolder, "settings.json")
	if _, err := os.Stat(settingsPath); os.IsNotExist(err) {
		Settings.ClientToken = uuid.New()
		return SaveSettings(rootFolder)
	} else {
		f, err := os.Open(settingsPath)
		if err != nil {
			return err
		}
		defer f.Close()

		return json.NewDecoder(f).Decode(&Settings)
	}
	return nil
}

func SaveSettings(rootFolder string) error {
	settingsPath := path.Join(rootFolder, "settings.json")

	f, err := os.Create(settingsPath)
	if err != nil {
		return err
	}
	defer f.Close()

	return json.NewEncoder(f).Encode(Settings)
}
